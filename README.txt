The PDF Archive module provides the ability to generate PDF archives of any
entity triggered by Rules actions. Both the entity view mode and role of the
simulated user used for rendering the entity can be set per rule.

An example feature is included to demonstrate the configuration of this module.

PDF Archive was written Stuart Clark and is maintained by Stuart Clark
(deciphered) and Brian Gilbert (realityloop) of Realityloop Pty Ltd.
- http://www.realityloop.com
- http://twitter.com/realityloop

PDF Archive development was initially sponsored by the Lab4 Web Design.
- http://lab4.com.au



Required Modules
--------------------------------------------------------------------------------

* Libraries API - http://drupal.org/project/libraries
* Rules         - http://drupal.org/project/rules



Installation
--------------------------------------------------------------------------------

1. Download the TCPDF library from
   http://downloads.sourceforge.net/project/tcpdf/tcpdf_5_9_189.zip and extract
   it to your Libraries folder as 'tcpdf', eg. '/sites/all/libraries/tcpdf'.

2. Install module as usual, see http://drupal.org/node/895232 for further
   information.



Makefile entries
--------------------------------------------------------------------------------

For easy downloading of Custom Formatters and it's required/recommended modules
and/or libraries, you can use the following entries in your makefile:


  projects[entity][subdir] = contrib
  projects[entity][version] = 1.5

  projects[libraries][subdir] = contrib
  projects[libraries][version] = 2.2

  projects[rules][subdir] = contrib
  projects[rules][version] = 2.7

  libraries[tcpdf][download][type] = get
  libraries[tcpdf][download][url] = http://downloads.sourceforge.net/project/tcpdf/tcpdf_5_9_189.zip


